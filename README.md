# Max Sum Sub Array

This program will take a list of positive and negative integers and find the sub-array of contiguous values with the largest sum.