#include "subarray.h"

void error(string message)
{
    fprintf(stderr, "%s", message);
    exit(1);
}

int is_positive(int number)
{
    return number > 0;
}

int samesign(int a, int b)
{
    return (is_positive(a) && is_positive(b)) || (!is_positive(a) && !is_positive(b));
}

void make_list(subarraylist * out, int * array, unsigned int length)
{
    if (length < 1)
    {
        error("length must be greater than zero in make_list");
    }

    subarraylist list = (subarraylist) {
        .head = (subarray *)malloc(length * sizeof(subarray)),
        .length = length
    };

    if (list.head == NULL)
    {
        error("malloc failed in make_list\n");
    }

    for (int i = 0; i < length; i++)
    {
        list.head[i].start = i;
        list.head[i].end = i;
        list.head[i].sum = array[i];
    }

    *out = list;
}

void prune_edges(subarraylist * out)
{
    subarraylist list = *out;
    subarraylist newlist;

    int newstart = 0;

    if (list.length < 3)
    {
        return;
    }

    if (!is_positive(list.head[0].sum))
    {
        newstart = 1;
        newlist.length--;
    }

    if (!is_positive(list.head[list.length - 1].sum))
    {
        newlist.length--;
    }

    if (list.length == newlist.length)
    {
        return;
    }

    newlist.head = (subarray *)malloc(newlist.length * sizeof(subarray));

    if (newlist.head == NULL)
    {
        error("malloc failed in prune_edges\n");
    }

    for (int i = 0; i < newlist.length; i++)
    {
        newlist.head[i] = list.head[newstart + i];
    }

    free(list.head);
    *out = newlist;
}

/* helper function for combine_adjacents */
static int get_length_combine_adjacents(subarraylist list)
{
    int newlength = 1;
    for (int i = 0; i < list.length - 1; i++)
    {
        if (!samesign(list.head[i].sum, list.head[i + 1].sum))
        {
            newlength++;
        }
    }
    return newlength;
}
void combine_adjacents(subarraylist * out)
{
    subarraylist list = *out;

    if (list.length < 2)
    {
        return;
    }

    subarraylist newlist;
    newlist.length = get_length_combine_adjacents(list);

    if (newlist.length >= list.length)
    {
        return;
    }

    newlist.head = (subarray *)malloc(newlist.length * sizeof(subarray));

    if (newlist.head == NULL)
    {
        error("malloc failed in combine_adjacents\n");
    }

    newlist.head[0].sum = 0;
    newlist.head[0].start = list.head[0].start;
    for (int i = 0, ni = 0 ;; i++)
    {
        newlist.head[ni].sum += list.head[i].sum;

        if (i == list.length - 1)
        {
            newlist.head[ni].end = list.head[i].end;
            break;
        }

        if (!samesign(list.head[i].sum, list.head[i + 1].sum))
        {
            newlist.head[ni].end = list.head[i].end;
            ni++;
            newlist.head[ni].start = list.head[i + 1].start;
            newlist.head[ni].sum = 0;
        }
    }

    free(list.head);
    *out = newlist;
}
