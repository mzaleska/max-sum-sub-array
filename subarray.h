#ifndef _SUBARRAY_H_
#define _SUBARRAY_H_

#include <stdio.h>
#include <stdlib.h>

typedef const char * string;
typedef struct subarray subarray;
typedef struct subarraylist subarraylist;

struct subarray
{
    int sum;
    unsigned int start;
    unsigned int end;
};

struct subarraylist
{
    subarray * head;
    unsigned int length;
};

/* prints error message to stderror and exists program */
void error(string message);

/* returns 1 if number is greater than 0 */
int is_positive(int number);

/* returns 1 if a and b have the same sign */
int samesign(int a, int b);

/* instantiates a subarraylist for the specified integer array */
void make_list(subarraylist * out, int * array, unsigned int length);

/* removes negative sum subarrays from the edges of the list */
void prune_edges(subarraylist * out);

/* combines adjacent subarrays which have the same sign */
void combine_adjacents(subarraylist * out);

#endif/*_SUBARRAY_H_*/