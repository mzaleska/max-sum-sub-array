#include "tests.h"

static int array[] = { 8, 2, -11, 5, 40, -6, 2, -5, 2, -3, 30, 90, -2, 1 };
static unsigned int length = 14;
static int edge_array[] = { -1, -2, 5, 3, -1, -4, 5, 6, -2, -9 };
static unsigned int edge_array_length = 10;

void printlist(subarraylist list)
{
    if (list.head == NULL || list.length < 1)
    {
        printf("list empty\n");
        return;
    }

    for (int i = 0; i < list.length; i++)
    {
        printf("index: %d, start: %d, end: %d, sum: %d\n", i, list.head[i].start, list.head[i].end, list.head[i].sum);
    }
}

enum testcode test_make_list()
{
    subarraylist list;
    make_list(&list, array, length);

    if (list.head == NULL)
    {
        return failure;
    }

    printlist(list);

    free(list.head);
    return success;
}

enum testcode test_prune_edges()
{
    subarraylist list;
    make_list(&list, edge_array, edge_array_length);

    if (list.head == NULL)
    {
        return failure;
    }

    printf("Before:\n");
    printlist(list);
    putchar('\n');

    combine_adjacents(&list);

    printf("Combined:\n");
    printlist(list);
    putchar('\n');

    prune_edges(&list);

    printf("Pruned:\n");
    printlist(list);
    putchar('\n');

    free(list.head);
    return success;
}

enum testcode test_combine_adjacents()
{
    subarraylist list;
    make_list(&list, array, length);

    if (list.head == NULL)
    {
        return failure;
    }

    printf("Before:\n");
    printlist(list);
    putchar('\n');

    combine_adjacents(&list);

    printf("After:\n");
    printlist(list);
    putchar('\n');

    free(list.head);
    return success;
}