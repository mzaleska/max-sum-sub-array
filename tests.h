#ifndef _TESTS_H_
#define _TESTS_H_

#include "subarray.h"

enum testcode
{
    failure = 0,
    success = 1
};

void printlist(subarraylist list);
enum testcode test_make_list();
enum testcode test_prune_edges();
enum testcode test_combine_adjacents();

#endif/*_TESTS_H_*/